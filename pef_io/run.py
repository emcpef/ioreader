import time
import datetime
import os
#from serializers import mqtt
from serializers.files import Serializer
from gpios import GPIO

VERSION = "3.0"

MODULE = 'vf'

if MODULE == 'vf':
    gpio1 = GPIO(135, 89, "in")
    gpio2 = GPIO(98, 46, "in")
    gpio3 = GPIO(133, 88, "in")
    gpio4 = GPIO(103, 48, "in")
    gpio5 = GPIO(101, 47, "in")

    gpio6 = GPIO(97, 50, "out")
    Serializer().mount(gpio6.alias)

    gpio7 = GPIO(85, 53, "out")
    Serializer().mount(gpio7.alias)

    gpio8 = GPIO(79, 49, "out")
    Serializer().mount(gpio8.alias)

#    gpio9 = GPIO(45, 41, "out")
#    Serializer().mount(gpio9.alias)

if MODULE == 'imx7':
    gpio1 = GPIO(135, 10, "in")
    gpio2 = GPIO(98, 171, "in")
    gpio3 = GPIO(133, 90, "in")
    gpio4 = GPIO(103, 145, "in")
    gpio5 = GPIO(101, 144, "in")

def check():
    gpio1.check()
    gpio2.check()
    gpio3.check()
    gpio4.check()
    gpio5.check()

def serialize():
    print("serialize")

    data = "{0},{1},{2},{3},{4}".format(gpio1.get_value(),
                                        gpio2.get_value(),
                                        gpio3.get_value(),
                                        gpio4.get_value(),
                                        gpio5.get_value())

    timestamp = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print(timestamp+data)

    Serializer().save(data,timestamp)

#    insert = "INSERT INTO DATA('POSTOID','Data', 'Dado') VALUES ('65', '{0}', '{1}')".format(
#    datetime.datetime.now(), values)
#    print(insert)
#    mqtt.single_publish(insert)

def write_status():
    data = "{0},{1},{2},{3},{4}".format(gpio1.get_value(),
                                        gpio2.get_value(),
                                        gpio3.get_value(),
                                        gpio4.get_value(),
                                        gpio5.get_value())

    timestamp = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    Serializer().save_status(data,timestamp)

def set_output():
    print("set_output")
    gpio6.set_value(Serializer().read(gpio6.alias))
    gpio7.set_value(Serializer().read(gpio7.alias))
    gpio8.set_value(Serializer().read(gpio8.alias))
#    gpio9.set_value(Serializer().read(gpio9.alias))

def check_disk():
    disk = os.statvfs("/")
    bfree = int(disk.f_bfree)
    bsize = int(disk.f_bsize)
    free = bfree*bsize
    meg = 1024*1024

    if(int(free/meg) < 20):
        Serializer().warning("Sem espaco disponivel na flash do equipamento")

def run():
    counter = 0
    while(True):
        counter += 1
        check()
        set_output()
        write_status()
        check_disk()
        time.sleep(0.3)
        if counter == 180:
            serialize()
            counter = 0

Serializer().start()
run()
