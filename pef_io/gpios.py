import time
import os.path
import json
#import platform

path = "/sys/class/gpio/"

arch = True
#machine = platform.machine()
#if machine == "armv7l" or machine == "armv7h":
#    arch = True
#else:
#    arch = False

class GPIO:
    def __init__(self, sodimm, number, direction):
        self.number = number
        self.alias = str(sodimm)
        self.direction = direction

        start_time = time.time()
        elapsed_time = time.time() - start_time
        export = path + "export"
        unexport = path + "unexport"

        self.value_path = path + "gpio" + str(number) + "/value"
        self.direction_path = path + "gpio" + str(number) + "/direction"
        self.active_low_path = path + "gpio" + str(number) + "/active_low"

        self.value = 0
        self.start_time = 0
        self.total_time = 0
        self.pulse_counter = 0

        if arch:
            if (os.path.exists(self.value_path)):
                f = open(unexport,"w")
                f.write(str(number))
                f.close()

            f = open(export,"w")
            f.write(str(number))
            f.close()
            
            if direction == "in":
                f = open(self.direction_path, "w")
                f.write("in")
                f.close()

            if direction == "out":
                f = open(self.direction_path, "w")
                f.write("out")
                f.close()

            f = open(self.active_low_path, "w")
            f.write("1")
            f.close()

        else:
            print("x86 arch, ignoring export gpio")
        
        print("started GPIO"+str(number))

    def check(self):
        if arch:
            f = open(self.value_path,"r")
            value = int(f.read())
            if value != self.value:
                self.value = value
                print("GPIO{0} value changed to {1}".format(self.alias, value))
                if value == 1:
                    self.start_time = time.time()
                    self.pulse_counter += 1
            if value == 0:
                self.start_time = 0
            else:
                self.total_time += time.time() - self.start_time
            f.close()
        else:
            print("x86 platform!")
            value = self.value
        return value

    def get_value(self):
        name = "GPIO{0}".format(self.alias)
        dict = {name: {"status": self.value, "time_on": '%.2f' % self.total_time, "pulse_counter": self.pulse_counter}}
        dict = json.dumps(dict)
        return dict
    
    def set_value(self, value):
        if self.direction == "in":
            return "error: not out direction"
        else:
            print("Set Value " +  self.value_path +" ---> " +str(value))
            if arch:
                f = open(self.value_path, "w")
                f.write(str(value))
                f.close()
            else:
                print("x86 platform!")
            return  value
