import datetime
import json
import pyodbc

url = 'localhost'
port = '1433'
database = 'AppDB'
username = 'SA'
password =  'Cola!1234' 
driver = 'DRIVER=FreeTDS' 

try:
    cnxn_string = driver+';SERVER='+url+';PORT='+port+';DATABASE='+database+';UID='+username+';PWD='+ password+";TDS_Version=8.0;"
    print(cnxn_string)
    cnxn = pyodbc.connect(cnxn_string)
except pyodbc.Error as ex:
    sqlstate = ex.args[0]
    print(ex)
    print("DB connect error: ", sqlstate)

print(cnxn)
cursor = cnxn.cursor()

#Sample select query
cursor.execute("SELECT * FROM PostoBigDatas;")
row = cursor.fetchone()
while row:
        print (row)
        row = cursor.fetchone()

now = datetime.datetime.now()
now = now.strftime("%Y-%m-%d %H:%M:%S")

IO1 = {'IO1':{'value':1, 'tempo': 20 }}
IO1 = json.dumps(IO1)
IO2 = {'IO2':{'value':1, 'tempo': 30 }}
IO2 = json.dumps(IO2)
IO3 = {'IO3':{'value':1, 'tempo': 40 }}
IO3 = json.dumps(IO3)
IO4 = {'IO4':{'value':1, 'tempo': 50 }}
IO4 = json.dumps(IO4)

AN1 = {'AN1':{'value':1 }}
AN1 = json.dumps(AN1)
AN2 = {'AN2':{'value':1 }}
AN2 = json.dumps(AN2)
AN3 = {'AN3':{'value':1 }}
AN3 = json.dumps(AN3)
AN4 = {'AN4':{'value':1 }}
AN4 = json.dumps(AN4)

Data = {"Data": now}
Data = json.dumps(Data)

valuestr = Data + ',' + IO1 + ',' +  IO2 + ',' + IO3 + ',' + IO4 + ',' + AN1 + ',' + AN2 + ',' + AN3 + ',' + AN4

#cursor.execute("INSERT INTO PostoBigDatas(PostoID, Data, Dado) VALUES ('{0}','{1}','{2}')".format(65,now,valuestr))
#cnxn.commit()

cursor.execute("SELECT * FROM PostoBigDatas;")
row = cursor.fetchone()
while row:
        print (row)
        row = cursor.fetchone()
