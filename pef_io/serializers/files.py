import datetime

class Serializer:
    def __init__(self, path):
        self.path = path

    def __init__(self):
        self.path = "/srv/log/"

    def start(self):
        ts = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        name = datetime.datetime.now().strftime("%d-%m-%Y")
        with open(self.path + name , 'a') as file:
            data = ts + ', START\n'
            file.write(data)
        file.close()

    def save(self,d,ts):
        name = datetime.datetime.now().strftime("%d-%m-%Y")
        with open(self.path + name , 'a') as file:
            data = ts + ',' + d + '\n\r'
            file.write(data)
        file.close()

    def save_status(self,d,ts):
        name = "gpio_status"
        with open(self.path + name , 'w') as file:
            data = ts + ',' + d
            file.write(data)
        file.close()

    def warning(self, warn):
        name ="ERRO"
        with open(self.path + name , 'w') as file:
            data = warn 
            file.write(data)
        file.close()

    def read(self,gpio_name):
        with open(self.path + "sodimm" + gpio_name, 'r') as file:
            value = file.read()
        file.close()
        return value

    def mount(self, gpio_name):
        with open(self.path + "sodimm" + gpio_name, 'w') as file:
            file.write(str(0))
        file.close()