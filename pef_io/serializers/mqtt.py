import time
import json
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt

SERVER_URL = "test.mosquitto.org"
SERVER_PORT = 1883
CLIENT_ID = "0001"
USERNAME = "teste"
PASSWORD = "teste"
SUB_TOPIC = "pef/test"

def single_publish(command):
    publish.single(
        SUB_TOPIC,
        payload=command,
        hostname=SERVER_URL,
        port=SERVER_PORT,
        client_id=CLIENT_ID,
#        keepalive=60,
#        will=None,
        protocol=mqtt.MQTTv311,
        transport="tcp")

if __name__ == "__main__":
    log = json.dumps({"teste": 1})
    print(log)
    single_publish(log)

#   while 1:
#       print(log)
#       single_publish(log)
#       time.sleep(1)
